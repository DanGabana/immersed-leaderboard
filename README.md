# Leaderboard Challenge

Made by Daniel Gabana - 10/02/2020


:: HOW TO USE IT ::

This project was developed with Unity 2019.1.13.f1. Once you Play the project, you have to press the key ‘L’ to show or hide the board.

The testing data used in this challenge is about the Spanish soccer league ‘La Liga’. You can find this data in Assets/StreamingAssets/DataTest.json. The data of each team has 5 columns: Team name, Points, Matches Won, Matches Lost and Matches Draw. You can sort the data based on any of these columns by clicking on the arrows below the title of each column. The data can be sorted in ascending or descending order.

The WorldSpaceCanvas holds the Leaderboard and the LoadingText GameObjects. Whilst the first object contains all the necessary GameObject and components for the Leaderboard, the LoadingText is used to let the user know the current status of the data. If there’s an error while loading it, an error message will display. Ideally, this message should have a ‘Refresh’ button attempt loading the data again.

NOTE: I included a ScreenSpace canvas in the LeaderboardScene for you to test the data management in two different scenes. To test it, you just have to enable the GameObject ScreenSpaceCanvas, play the scene and click on the button at the bottom of the screen. This will take you to Scene2, where you can see a log in the Console with the league’s name. If you want to further explore the data in this new scene, just select the ScriptableObject referenced in the TestChangeScene script attached to the _TestChangeScene GameObject.



:: THE CODE ::

In this challenge I decided to use an MVC (Model-View-Controller) design pattern. I like this design patter because it lets you organise the code in a very simple way that is easy to scale up. If different members of the team are going to work on the same project, they can split up the tasks and work on different views, controllers or models. This design pattern also allows to isolate each functionality such as the data management, the view management or the interaction.

My approach in this coding challenge was to create a structure that is scalable, thinking that other developers in my team may take this project in the future. Thus, I created some scripts that may look a bit empty such as the InteractionController and the ViewController. If we want to add more functionalities to this project, it would be easy for the new developer to understand its structure and start coding on the existing controllers. It is important to note that the InteractionController and ViewController are attached to the _Controllers GameObject in scene LeaderboardScene just in case more functionality is added. I could have these controllers not attached to a GameObject (as I did with the DataController) but the MainController would have to have references to certain elements that should not belong to the it such as the Leaderboard View or interaction events.

I could have created a LeaderboardController that controls the whole process of loading data and displaying the view, but I want to keep this a bit simpler. If this project would be bigger, a LeaderboardController would be required for an optimal code structure and organisation. Furthermore, I would like to say that I have not created interfaces or inheritance as I don’t think it was necessary for this small challenge. You may disagree with this decision (needless to say it is a completely valid opinion!), but since it is very simple and there are not variants of the same data or scripts, I decided to keep it simple and easy for you to read.

The data used in this project (Assets/StreamingAssets/DataTest.json) is loaded from the DataController script. This script stores the data in a ScriptableObject called LeaderboardData, placed in the Assets/Data folder. I could have taken the _Controllers GameObject to other scenes, making it a singleton and using Unity’s function DontDestroyOnLoad. However, this is not necessary for this challenge. To add more columns to this data (either to the league’s information or to each Team), the user just needs to add the desired columns (also known as attributes or properties) the LeaderboardData or TeamData scripts as well as to the DataTest.json file. Obviously, the properties need to have the exact same name. Note I added getters and setters for each property of the TeamData just in case we need to do some verification or data manipulation before setting the data. 

The data structure is very simple. It contains 2 attributes in the root (league and seasson) that store generic data about the leaderboard. There’s a third attribute named teams that contains an array or list of the team objects, created dynamically in the leaderboard. Each team object contains 5 attributes: name, points, won, lost and draw. This structure is very flexible as users can easily add or delete objects in the teams array, which will be automatically displayed in the leaderboard.

If the user wants to load the data from the server, s/he can enable the toggle TestServerData in the MainController script. Since the server is not returning any data, this will throw an exception in Unity. I included a compilation directive (see DataController) so the testing data is only used within the editor. When the project is built for any platform, it will use the server’s data. I could make the directives more specific so it only uses the server’s data when building for a specific platform (i.e.: Android). Furthermore, I left commented some code in the DataController that checks which VR device is currently being used.

Finally, I put all the assets I am using (LeanTween, Skyboxes and TextMesh Pro) in the Standard Assets folder. This is to speed up the Editor compilation time. Watch this video (https://www.youtube.com/watch?v=pArrdWr6cIM) for details (interesting bit comes after 7:45)

Regarding the Skybox – I like the one I used in LeadeboardScene, although they greyed out the bottom part of the picture for some reason and it looks a bit odd… I used a different skybox in Scene2, but it has a low resolution so it may look a bit weird! I must say I wanted to put a Stadium or something related to sports but I couldn’t find any free skybox with this theme!

I could have added more functionalities and other stuff such as loading the teams’ logo, putting a search bar or adding more UI. Instead of adding these, I decided to make it a bit more visually appealing introducing some animations. I strongly believe that animations are an important part of UI/UX design as they can convey important information for the user. 
