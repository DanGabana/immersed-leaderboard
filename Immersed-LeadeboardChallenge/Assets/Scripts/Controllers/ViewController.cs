﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ViewController : MonoBehaviour
{
    // Leaderboard view*
    [SerializeField]
    private LeaderboardView _leadrboardView;

    // Text showing "Loading..." or "ERROR!"*
    [SerializeField]
    private GameObject _loadingText;

    // Initialize the leaderboard with data*
    public void InitLeaderboard(LeaderboardData data)
    {
        _leadrboardView.Init(data);
    }

    //Show or hide the loading text. This function also changes the text to an Error message
    public void LoadingToggle(bool show, string msg = "")
    {
        _loadingText.SetActive(show);
        if (msg != "")
            _loadingText.GetComponent<TextMeshProUGUI>().text = msg;
    }

    // NOTE: The fields with * could be moved to a LeaderboardController if needed
}
