﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InteractionController : MonoBehaviour
{
    [Header("Event to Show/Hide the Leaderboard")]
    public UnityEvent KeyPressed;

    // Check when the user presses the L key
    public void CheckInteraction()
    {
        if (Input.GetKeyDown("l"))
        {
            // Invoke the Event so anyone subscribed to it knows the key has been pressed
            KeyPressed.Invoke();
        }
    }
}
