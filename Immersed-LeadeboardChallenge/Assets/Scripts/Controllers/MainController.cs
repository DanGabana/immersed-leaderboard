﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainController : MonoBehaviour
{
    public bool TestServerData = false;

    // Controllers
    private DataController _dataController;
    private ViewController _viewController;
    private InteractionController _interactionController;

    // Data
    [SerializeField]
    private LeaderboardData _dataLeaderBoard;

    void Start()
    {
        // Get the other controllers in this gameobject.
        // I think Views and Interaction controllers should be attached to a GameObject
        // to avoid having too many references in the MainController... Although of course there's many ways of doing this! 
        _interactionController = gameObject.GetComponent<InteractionController>();
        _viewController = gameObject.GetComponent<ViewController>();

        // DataController is created here in case we need to do some additional setup to this controller
        _dataController = new DataController();
        LoadData();
    }

    // It is good practice (and more efficient) to just have 1 Update method insted of multiples
    // so this Update method will call any other methods that require being updated every frame.
    private void Update()
    {
        _interactionController.CheckInteraction();
    }

    /// //////////////////////////////////////////////
    /// ////////////////////////////////////////////// DATA MANAGEMENT
    /// //////////////////////////////////////////////

    #region DATA MANAGEMENT

    // Load data and display the Loading text
    private void LoadData()
    {
        _viewController.LoadingToggle(true, "Loading...");
        // Data needs to be loaded in a Coroutine as it is asynchronous.
        // Note the last parameter is a callback function
        StartCoroutine(_dataController.LoadData(TestServerData, _dataLeaderBoard, DataLoaded));
    }

    // Callback function when data is loaded or there's an error
    private void DataLoaded(bool success)
    {
        // Start the view when data was loaded successfully. Otherwise show Error message
        if (success)
        {
            StartView();
            _viewController.LoadingToggle(false);
        }
        else
            _viewController.LoadingToggle(true, "ERROR! Data not loaded...");
    }

    #endregion

    /// //////////////////////////////////////////////
    /// ////////////////////////////////////////////// VIEW MANAGEMENT
    /// //////////////////////////////////////////////

    #region VIEW MANAGEMENT

    // Let the ViewController initialize and populate the views with the data loaded
    private void StartView()
    {
        _viewController.InitLeaderboard(_dataLeaderBoard);
    }
    
    #endregion

    /// //////////////////////////////////////////////
    /// ////////////////////////////////////////////// OTHER FUNCTIONS
    /// //////////////////////////////////////////////

        // This is just to test how the data id handled when changing scenes
    public void OnClick()
    {
        SceneManager.LoadScene("Scene2");
    }

}
