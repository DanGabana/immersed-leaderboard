﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.XR;

public class DataController
{
    public IEnumerator LoadData(bool testLiveDataFromEditor, LeaderboardData targetLeaderboardData, Action<bool> callbackResponse)
    {
        string url = "";

        // Compilation directive to load dummy (local) or live (server) data
#if UNITY_EDITOR

        // User can test live (server) data from the Editor too
        if(!testLiveDataFromEditor)
            url = "file://" + Application.streamingAssetsPath + "/DataTest.json";
        else
            url = "https://immersedvr.com/test/leaderboard"; // test with Live data from editor
#else
        url = "https://immersedvr.com/test/leaderboard";

        // I leave this commented since this is not going to be tested in VR (for now!)
        //if (XRSettings.enabled && XRSettings.isDeviceActive)
        //{
            // We can even check what device is being used!
            //if (XRSettings.loadedDeviceName == "Oculus")
            //    url = "https://immersedvr.com/test/leaderboard";
        //}

#endif
        // I could load the local data using File.Read but i prefer to keep it simple and do it this way!

        // Make request to load data
        UnityWebRequest www = UnityWebRequest.Get(url);
        www.method = "GET";

        //Send request
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            // If there's an error when loading the data, show an appropriate message and throw an Exception
            Debug.Log(www.error);
            callbackResponse(false);
            throw new Exception("** ERROR TRYING TO LOAD DATA FROM THE SERVER!");
        }
        else
        {
            // If the data was loaded successfully, overwrite the LeaderBoard data in Assets!
            JsonUtility.FromJsonOverwrite(www.downloadHandler.text, targetLeaderboardData);

            // Callback when data was loaded
            if (www.downloadHandler.text != "")
                callbackResponse(true);
            else
                callbackResponse(false);
        }
    }
        
}
