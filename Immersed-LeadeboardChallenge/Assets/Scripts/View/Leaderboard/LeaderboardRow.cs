﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LeaderboardRow : MonoBehaviour
{
    // TextFields
    [SerializeField]
    private TextMeshProUGUI _teamText;

    [SerializeField]
    private TextMeshProUGUI _pointsText;

    [SerializeField]
    private TextMeshProUGUI _wonText;

    [SerializeField]
    private TextMeshProUGUI _lostText;

    [SerializeField]
    private TextMeshProUGUI _drawText;

    // Populate the row with data
    public void PopulateRow(TeamData dataTeam)
    {
        _teamText.text = dataTeam.Name;
        _pointsText.text = dataTeam.Points.ToString();
        _wonText.text = dataTeam.Won.ToString();
        _lostText.text = dataTeam.Lost.ToString();
        _drawText.text = dataTeam.Draw.ToString();
    }

}
