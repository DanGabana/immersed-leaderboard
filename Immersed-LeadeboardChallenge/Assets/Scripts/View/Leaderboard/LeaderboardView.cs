﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class LeaderboardView : MonoBehaviour
{
    private LeaderboardData _leaderboardData;

    // Elements in the leaderboard
    [SerializeField]
    private TextMeshProUGUI _leagueText;
    [SerializeField]
    private TextMeshProUGUI _seassonText;
    [SerializeField]
    private GameObject _rowBoardPrefab;
    [SerializeField]
    private GameObject _containerBoardRows;

    // Color to highlight the text when sorting columns
    [SerializeField]
    private Color _highlightSelectedColor;

    // This field is to easily swap between categories to order by
    [SerializeField]
    [Header("Button to sort the board")]
    [Tooltip("The category and order to sort is taken automatically from the target button")]
    private Button _selectedButton;

    // Variables to specify how to sort the columns
    private int _orderBy = 2;
    private bool _descending = true; // true = descend; false = ascend

    // Text of the selected column to sort the data
    private TextMeshProUGUI _selectedText;

    private bool _shown = false;
    private float _timeAnimation = 0.8f;
    
    private void Awake()
    {
        // Reset the object's view at start
        gameObject.transform.Rotate(0, -40, 0, Space.World);
        gameObject.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0, 150, 0);
        gameObject.GetComponent<CanvasGroup>().alpha = 0;
        gameObject.GetComponent<RectTransform>().localScale = new Vector3(0.5f, 0.5f, 0.5f);
    }

    // Function called by the ViewController to initialize and populate the leaderboard
    public void Init(LeaderboardData data)
    {
        // delete existing rows
        FlushView();

        _leaderboardData = data;

        // Get and change the button specified to default sort the data
        SelectedButtonAndText(_selectedButton);
        _selectedButton.Select(); // Select it for first time

        // Get automatically the attribute to sort by and order from the target button.
        // Maybe this is not great because of people changing the GO's hierarchy order,
        // but i think it was nice to have instead of having it hardcoded or making more public variables
        _orderBy = _selectedButton.transform.parent.transform.GetSiblingIndex() + 1;
        _descending = _selectedButton.transform.GetSiblingIndex() == 1 ? true : false;


        PopulateBoardTitles();
        CreateBoardRows();
    }

    // to show or hide the leaderboard
    public void ToggleLeaderboardView()
    {
        if (!_shown)
            ShowLeaderboard();
        else
            HideLeaderboard();
    }

    // Function called by all the buttons to sort the data. Each button will pass a parameter
    public void OnClickButtonSort(int numButton)
    {
        // If the parameter is a negative number, sort the data in descending order, otherwise in ascending order
        _descending = numButton < 0 ? true : false;

        // Specifies the column to order by
        _orderBy = numButton < 0 ? numButton * -1 : numButton;

        // Get the clicked button and tint its text
        GameObject go = EventSystem.current.currentSelectedGameObject;
        SelectedButtonAndText(go.GetComponent<Button>());

        // refresh the board
        RefreshBoard();
    }

    /// /////////////////////////////////////////
    /// ///////////////////////////////////////// Private methods
    /// /////////////////////////////////////////

    // Show leaderboard
    public void ShowLeaderboard()
    {
        LeanTween.rotateY(gameObject, 0, _timeAnimation).setEase(LeanTweenType.easeOutSine);
        LeanTween.moveY(gameObject, 0, _timeAnimation).setEase(LeanTweenType.easeOutSine);
        LeanTween.alphaCanvas(gameObject.GetComponent<CanvasGroup>(), 1f, _timeAnimation);
        LeanTween.scale(gameObject.GetComponent<RectTransform>(), new Vector3(1f, 1f, 1f), _timeAnimation - 0.3f).setEase(LeanTweenType.easeOutSine);

        _shown = true;
    }

    // Hide leaderboard
    public void HideLeaderboard()
    {
        // Cancel tweens if they are still tweening when user hides the board
        LeanTween.cancel(gameObject);

        LeanTween.rotateY(gameObject, -40f, _timeAnimation * 0.6f).setEase(LeanTweenType.easeOutSine);
        LeanTween.moveY(gameObject, 150f, _timeAnimation * 0.6f).setEase(LeanTweenType.easeOutSine);
        LeanTween.alphaCanvas(gameObject.GetComponent<CanvasGroup>(), 0f, _timeAnimation * 0.3f);
        LeanTween.scale(gameObject.GetComponent<RectTransform>(), new Vector3(0.5f, 0.5f, 0.5f), _timeAnimation * 0.6f).setEase(LeanTweenType.easeOutSine);

        _shown = false;
    }

    // Sort leaderboard
    private List<TeamData> SortLeaderboard()
    {
        LeaderboardData newBoardSorted = new LeaderboardData();
        List<TeamData> newList = new List<TeamData>();

        // Maybe there's a more elegant way of doing this, but i want to keep it this way
        // so if another team member takes this code, can quickly understand it!
        switch (_orderBy)
        {
            case 1:
                newList = _leaderboardData.teams.OrderBy(w => w.Name).ToList();
                break;
            case 2:
                newList = _leaderboardData.teams.OrderBy(w => w.Points).ToList();
                break;
            case 3:
                newList = _leaderboardData.teams.OrderBy(w => w.Won).ToList();
                break;
            case 4:
                newList = _leaderboardData.teams.OrderBy(w => w.Lost).ToList();
                break;
            case 5:
                newList = _leaderboardData.teams.OrderBy(w => w.Draw).ToList();
                break;
            default:
                // Just in case!
                newList = _leaderboardData.teams.OrderBy(w => w.Points).ToList();
                break;
        }

        // Reverse the list if it is in descending order
        if (_descending)
            newList.Reverse();

        return newList;
    }

    private void FlushView()
    {
        // Delete all rows in the container
        foreach (Transform t in _containerBoardRows.transform)
        {
            Destroy(t.gameObject);
        }
    }

    // Populate 'static' textfields in the board
    private void PopulateBoardTitles()
    {
        _leagueText.text = _leaderboardData.league;
        _seassonText.text = "Seasson: "+_leaderboardData.seasson;
    }

    // Create new rows
    private void CreateBoardRows()
    {
        // First, sort the data
        List<TeamData> newList = SortLeaderboard();
        int i = 0;

        // Create rows in the order
        foreach (TeamData tData in newList)
        {
            GameObject newRow = Instantiate(_rowBoardPrefab, _containerBoardRows.transform);
            LeaderboardRow teamRow = newRow.GetComponent<LeaderboardRow>();
            teamRow.PopulateRow(tData);
            // Hide the row and then display them in order, it makes a nice visual effect :)
            teamRow.GetComponent<CanvasGroup>().alpha = 0;
            LeanTween.alphaCanvas(teamRow.GetComponent<CanvasGroup>(), 1f, 0.5f).setDelay(0.3f * i);
            i += 1;
        }
    }

    // Refresh the board when sorting by another value
    private void RefreshBoard()
    {
        FlushView();
        CreateBoardRows();
    }

    private void SelectedButtonAndText(Button buttonClicked)
    {
        // Tint black the previous selected column
        TextMeshProUGUI previousText = _selectedButton.transform.parent.GetComponent<TextMeshProUGUI>();
        previousText.color = new Color(0, 0, 0);

        // Tint the text in the selected column
        TextMeshProUGUI selectedText = buttonClicked.transform.parent.GetComponent<TextMeshProUGUI>();
        selectedText.color = _highlightSelectedColor;

        // Enable the previous button clicked
        _selectedButton.interactable = true;

        // Keep a reference and disable the current button clicked
        _selectedButton = buttonClicked;
        buttonClicked.interactable = false;
    }
}
