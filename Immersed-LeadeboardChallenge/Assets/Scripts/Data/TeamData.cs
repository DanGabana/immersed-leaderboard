﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// this class need to be serializable!
[System.Serializable]
public class TeamData 
{
    // Note the 'new', it's to not confuse with the object's native name property
    public new string name;

    public int points;
    public int won;
    public int lost;
    public int draw;

    // So far we don't really need getters and setters as the variables need to be public,
    // but it's always a good practice :)

    public string Name
    {
        get { return name; }
        set { name = value; }
    }

    public int Points
    {
        get { return points; }
        set { points = value; }
    }

    public int Won
    {
        get { return won; }
        set { won = value; }
    }

    public int Lost
    {
        get { return lost; }
        set { lost = value; }
    }

    public int Draw
    {
        get { return draw; }
        set { draw = value; }
    }
}
