﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

// This directive will allow us to create a new LeadboardData by right clicking on the Project Menu
[CreateAssetMenu(fileName = "LeaderboardData", menuName = "New Leaderboard Data")]
[System.Serializable]
public class LeaderboardData : ScriptableObject
{
    // Public properties are necessary for serialization
    public string league;
    public string seasson;

    public List<TeamData> teams = new List<TeamData>();

}
